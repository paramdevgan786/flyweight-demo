﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ZombieStats")]
public class ZombieData : ScriptableObject
{
    public int maxHealth;
    public int damagePower;
    public int wanderRadius;
    public int minWanderTimer;
}



