﻿using UnityEngine.AI;
using UnityEngine;

public enum ZombieState
{
    WANDER,
    //wander while on Nav Mesh is still
    IDLE
};

public class ZombieController : MonoBehaviour
{

    [SerializeField]
    private ZombieData zombieStats;
    //ref to nav mesh agent
    private NavMeshAgent agent;
    //ref to wander time
    private float localTimer = 0.0f;
    //ref to anim
    private Animator anim;
    //ref to zombie states
    private ZombieState state;
    //max random wander time
    private int maxWanderTime;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        maxWanderTime = zombieStats.minWanderTimer + 6;
    }

    private void Update()
    {
        localTimer += Time.deltaTime;
        controlMovement();

    }

    private void controlMovement()
    {
        if (localTimer >= UnityEngine.Random.Range(zombieStats.minWanderTimer, maxWanderTime))
        {
            Vector3 newPos = RandomNavSphere(transform.position, zombieStats.wanderRadius, -1);
            agent.SetDestination(newPos);
            anim.SetBool("isWalking", true);
            localTimer = 0;
        }
        else
        {
            if (agent.remainingDistance < 0.1f)
            {
                if (anim.GetBool("isWalking"))
                {
                    anim.SetBool("isWalking", false);
                }
            }
        }
    }


    private Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        NavMeshHit navHit;
        Vector3 randDirection = UnityEngine.Random.insideUnitSphere * dist;
        randDirection += origin;
        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }


}
